﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeChange : MonoBehaviour {

    public Slider thisSlider;
    public float volume;

    public void SetVolume()
    {
        volume = thisSlider.value;
        AkSoundEngine.SetRTPCValue("MasterVolume", volume);
    }
}
