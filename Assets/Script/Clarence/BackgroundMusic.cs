﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMusic : MonoBehaviour {

    public static BackgroundMusic Instance;

    private float nextActionTime = 0.0f;
    public float period = 1.0f;

    private float footstepsTimer = 0.0f;


    private void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        footstepsTimer += Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.L))
        {
            AkSoundEngine.PostEvent("LowPass", gameObject);
        }

        if (Input.GetAxis("JoystickHorizontal") != 0 || Input.GetAxis("JoystickVertical") != 0)
        {
            if (footstepsTimer > 0.8)
            {
                nextActionTime += period;
                Debug.Log("BOUM");
                BeginFootsteps();
                footstepsTimer = 0.0f;
            }
        }
        /*else
        {
            if (Time.time > nextActionTime)
            {
                nextActionTime += period;
                EndFootsteps();
            }
        }*/
    }
        
    


    public void EnterFireCamp()
    {
       
        AkSoundEngine.PostEvent("Light_Ambiance", gameObject);
    }

    public void TakeAHit()
    {

        AkSoundEngine.PostEvent("Ouch_Event", gameObject);
    }

    public void ExitFireCamp()
    {
        AkSoundEngine.PostEvent("Obscurity_Ambiance", gameObject);
    }

    public void BeginFootsteps()
    {
        AkSoundEngine.PostEvent("Footsteps_Event", gameObject);
    }

    public void EndFootsteps()
    {
        AkSoundEngine.PostEvent("Footsteps_stop", gameObject);
    }
}
