﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireCamp : MonoBehaviour {

	// Use this for initialization
	void Start () {
       // BackgroundMusic.Instance.ExitFireCamp();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            BackgroundMusic.Instance.EnterFireCamp();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            BackgroundMusic.Instance.ExitFireCamp();
        }
    }
}
