﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Day_Night : MonoBehaviour
{

    private float _nightTimer;
    public bool _isDayOn;
    public bool _isNightSet;
    private float _intensity;
    private float _skyboxExposure;



    // Use this for initialization
    void Start()
    {
        _nightTimer = 5f; // time before night begins
        _intensity = 0f;  // intensity of luminosity in the scene, by making it go down, the night appear max = 1
        _skyboxExposure = 0f; //Luminosity of the skybox max =1
        RenderSettings.skybox.SetFloat("_Exposure", _skyboxExposure); // reset la luminositté de la skybox
        RenderSettings.ambientIntensity = _intensity;  // reset l'intensité de la scène
    }

    // Update is called once per frame
    void Update()
    {


        if (_isDayOn)
        {
            _intensity += 0.1f;
            if (_intensity >= 0.5f)
            {
                _intensity = 0.5f;    // set le max d'intensité 
            }
            RenderSettings.ambientIntensity = _intensity; // control the intensity of the luminosity in the scene.
            /* _skyboxExposure += 0.005f;
             if (_skyboxExposure >= 1f)
             {
                 _skyboxExposure = 1f;   //set le max de luminosité de la skybox
             }
             _nightTimer -= Time.deltaTime;  // make the countdown for the night.
             RenderSettings.skybox.SetFloat("_Exposure", _skyboxExposure);
             RenderSettings.ambientIntensity = _intensity; // control the intensity of the luminosity in the scene.*/
        }

        if (!_isDayOn)
        {


            if (_intensity > 0.1f)
            {
                _intensity -= 0.1f;
            }
            else
            {
                _intensity = 0.1f;
            }
                // _skyboxExposure = 0.1f;
            RenderSettings.ambientIntensity = _intensity; // control the intensity of the luminosity in the scene.*/
                /*  _skyboxExposure -= 0.005f;
                  if (_skyboxExposure <= 0.1f)
                  {
                      _skyboxExposure = 0.1f;
                  }
                  RenderSettings.skybox.SetFloat("_Exposure", _skyboxExposure);
                  _intensity -= 0.005f; // make the night gradually appear
                  
                      _nightTimer = 10f;
                      _isNightSet = true; // check when the night has finished to set in order for the monster to be able to move
                  }
                  RenderSettings.ambientIntensity = _intensity; // control the intensity of the luminosity in the scene.*/
            }


        }



}