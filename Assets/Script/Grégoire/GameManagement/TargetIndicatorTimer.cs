﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetIndicatorTimer : MonoBehaviour {

    public float timer { private get; set; }
    public int _indicatorTime;
    public int stayOnScreenTime;

	// Use this for initialization
	void Start () {
        timer = 0;
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        if (timer > _indicatorTime)
        {
            this.GetComponent<TargetIndicator>().enabled = true;
            this.GetComponent<TargetIndicator>().m_iconImage.enabled = true;
            if (timer > _indicatorTime + stayOnScreenTime)
            {
                this.GetComponent<TargetIndicator>().enabled = false;
                this.GetComponent<TargetIndicator>().m_iconImage.enabled = false;
                timer = 0;
            }
        }
        else
        {
            this.GetComponent<TargetIndicator>().enabled = false;
            this.GetComponent<TargetIndicator>().m_iconImage.enabled = false;
        }
        
	}
}
