﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class particulFixed : MonoBehaviour {

    public GameObject _player;

    // Use this for initialization
    void Start () {
        this.transform.position = _player.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.position = Vector3.Lerp(this.transform.position, _player.transform.position, Time.deltaTime * 10);
    }
}
