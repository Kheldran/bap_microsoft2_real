﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camerafixed : MonoBehaviour {

    public GameObject player;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 pos = player.transform.position;            // prend la position du joueur
        transform.position = new Vector3(pos.x, pos.y, -25f);        // set la caméra sur la position du joueur en hauteur sur l'axe y
    }
}
