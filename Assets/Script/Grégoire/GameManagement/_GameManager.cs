﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class _GameManager : MonoBehaviour {

    public int _playerLife;             // vie du joueur
    public GameObject _player;			// gameobject du joueur
    public GameObject _blackScreen;
    public GameObject _blackScreen2;
    public GameObject _blackScreen2Second;
    public GameObject[] _enemyChangingByDifficulty;


    private GameObject _pauseMenu;
    private GameObject _pauseOptionMenu;
    public GameObject _backToPauseButton;
    public GameObject _resumeButton;


    public GameObject _DefeatScreen;
    public GameObject _replay;

    public AkBank mainBank;

    // Use this for initialization
    void Start () {
		_playerLife = 3;				// vie du joueur = 3
        _blackScreen.SetActive(false);
        _blackScreen2.SetActive(false);
        _blackScreen2Second.SetActive(false);
        //get le menu pause
        _pauseMenu = GameObject.FindGameObjectWithTag("Pause");
        _pauseOptionMenu = GameObject.FindGameObjectWithTag("PauseOption");

        //initialise le menu pause en caché
        _pauseMenu.SetActive(false);
        _pauseOptionMenu.SetActive(false);

        //active le menu de défaite
        _DefeatScreen.SetActive(false);	


        if (Menu._normalMode)
        {
            foreach (GameObject enemy in _enemyChangingByDifficulty)
            {
                enemy.SetActive(false);
            }
        }
        else if (Menu._hardMode)
        {
            foreach (GameObject enemy in _enemyChangingByDifficulty)
            {
                enemy.SetActive(true);
            }

        }
    }
	
	// Update is called once per frame
	void Update () {

        //active le menu pause ou non
        if (Input.GetKeyDown(KeyCode.Escape) && !_pauseMenu.activeSelf)
        {
            Time.timeScale = 0;
            _pauseMenu.SetActive(true);
            EventSystem.current.SetSelectedGameObject(_resumeButton);
        }
        else if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown("joystick button 1")) && _pauseMenu.activeSelf)
        {
            Time.timeScale = 1.0f;
            _pauseMenu.SetActive(false);
            _pauseOptionMenu.SetActive(false);
            _player.GetComponent<Move>().enabled = true;

        }

        //si le menu est activé, enleve les déplacement du joueurs, sinon les réactives
        if (_pauseMenu.activeSelf)
        {
            _player.GetComponent<Move>().enabled = false;
        }

        //si on appuie sur B dans le menu option pause, revient au menu pause
        if (_pauseOptionMenu.activeSelf && Input.GetKeyDown("joystick button 1"))
        {
            _pauseOptionMenu.SetActive(false);
        }
        

        //active les écrans de dégats selon la vie du joueur
        if (_playerLife == 2)
        {
            _blackScreen.SetActive(true);
        }

        if (_playerLife == 1)
        {
            _blackScreen2.SetActive(true);
            _blackScreen2Second.SetActive(true);
            
        }

        if (_playerLife <= 0) {
			
			Time.timeScale = 0f;				// met en pause le jeu
			_DefeatScreen.SetActive(true);					// cache le joueur
            EventSystem.current.SetSelectedGameObject(_replay);

		}

	}
    //restart the game from pause state
    public void ResumeGame()
    {
        Time.timeScale = 1.0f;
        _pauseMenu.SetActive(false);
    }
    //enable the pauseOptionMenu
    public void PauseOption()
    {
        _pauseOptionMenu.SetActive(true);
        EventSystem.current.SetSelectedGameObject(_backToPauseButton);
    }
    //go back to pause menu from pauseOptionMenu
    public void PauseOptionBack()
    {
        _pauseOptionMenu.SetActive(false);
        EventSystem.current.SetSelectedGameObject(_resumeButton);
    }

    //permet de quitter le jeu quand on clique sur le bouton QUIT
    public void OnButtonQuit()
    {
        Application.Quit();
    }

    //permet de revenir au menu quand on a perdu
    public void BackToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    //permet de rejouer quand on a perdu
    public void playAgain()
    {
        AkSoundEngine.StopAll();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        
    }


    //permet de changer la qualité du jeu dans les options
    public void QualityChange(int quality)
    {
        QualitySettings.SetQualityLevel(quality);
    }

    //permet de set le jeu en fullscreen ou non
    public void SetFullscreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }

}
