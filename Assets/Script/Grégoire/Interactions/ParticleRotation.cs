﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleRotation : MonoBehaviour
{
    public GameObject player;
    private Move _moveScript;
    private bool _isInThisFire;

    public Transform Target;
    public float RotationSpeed;

    //values for internal use
    private Quaternion _lookRotation;
    private Vector3 _direction;

    private void Start()
    {
        _moveScript = player.GetComponent<Move>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_isInThisFire && _moveScript._lanternDuration < _moveScript._lanternDurationMax)
        {
            if (!this.gameObject.GetComponent<ParticleSystem>().isPlaying)
            {
                this.gameObject.GetComponent<ParticleSystem>().Play();
            }
            Debug.Log("fire");
            Vector3 vectorToTarget = transform.position - Target.transform.position;

            //calcule la tangente des coordonnées x et y du vecteur ci-dessus, puis la convertit en degrée et lui rajoute 90 pour l'orienter dans le bon sens
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg + 90;
        
            //calcule une rotation de l'angle "angle" sur l'axis (0,0,1)
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);

            //fait rotate le gameObject selon l'angle q à une vitesse definie
            transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * RotationSpeed);
        }
        else if (!_isInThisFire || _moveScript._lanternDuration >= _moveScript._lanternDurationMax)
        {
            if (this.gameObject.GetComponent<ParticleSystem>().isPlaying)
            {
                this.gameObject.GetComponent<ParticleSystem>().Stop();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            _isInThisFire = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            _isInThisFire = false;
        }
    }
}