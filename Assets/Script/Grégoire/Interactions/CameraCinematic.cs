﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCinematic : MonoBehaviour {

    public GameObject objectToSee;
    public GameObject _camera;
    public GameObject _player;
    public GameObject _fog;

    private Camerafixed _cameraPlayerScript;
    private Move _moveScript;

    private bool _cinematic;
    public bool _isCameraGoingToTarget;
    private bool _isCameraGoingBack;

    private Vector3 cameraDoorPosition;
    private Vector3 playerPosition;

    private float startTime;
    private float _distance;
    private float _distance2;
    public float _speed;

	// Use this for initialization
	void Start () {
        // get the scripts of the camera which follows the player and the player movement
        _cameraPlayerScript = _camera.GetComponent<Camerafixed>();
        _moveScript = _player.GetComponent<Move>();

        //create a vector using the target characteristic while keeping the same camera height
        cameraDoorPosition = new Vector3 (objectToSee.transform.position.x, objectToSee.transform.position.y, -25);
        


        // Calculate the journey length.
        _distance = Vector3.Distance(playerPosition, cameraDoorPosition);

        // Calculate the journey length.
        _distance2 = Vector3.Distance(cameraDoorPosition, playerPosition);

        _fog.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {

        if (!_cinematic) { 
            // Keep a note of the time the movement started.
            startTime = Time.time;
        }
        if (_cinematic)
        {
            if (Time.time >= startTime + 1)
            { 
                // Distance moved = time * speed.
                float distCovered = (Time.time - startTime + 2) * _speed;

                // Fraction of journey completed = current distance divided by total distance.
                float fracJourney = distCovered / _distance;
                //make it so the camera move towards the target
                if (_isCameraGoingToTarget)
                {
                    _camera.transform.position = Vector3.Lerp(_camera.transform.position, cameraDoorPosition, fracJourney);
              
                }
            }
            // when the camera reaches the target, make ti so it can go back
            if (_camera.transform.position.x <= cameraDoorPosition.x + 0.01f && _camera.transform.position.y >= cameraDoorPosition.y - 0.01f)
            {
                _isCameraGoingToTarget = false;
                _isCameraGoingBack = true;
                
            }

            if (_isCameraGoingBack)
            {
                //float distCovered2 = (Time.time - startTime) * _speed;
                //float fracJourneyRetour = distCovered2 / _distance2;
                _camera.transform.position = playerPosition;   //retourne la caméra au joueur 
                _moveScript.enabled = true;                     // enable the movement script again 
                _cameraPlayerScript.enabled = true;             // enable the camera follow again
                this.gameObject.GetComponent<BoxCollider2D>().enabled = false;      //disable the trigger that created the cinematic
                _fog.SetActive(false);
            }
           

            
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //if the trigger meets the player, save the player position, disable the camera follow and movement script and make it so the camera moves to the target
        if (other.CompareTag("Player"))
        {
            playerPosition = new Vector3(_player.transform.position.x, _player.transform.position.y, -25);
            _isCameraGoingToTarget = true;
            _cameraPlayerScript.enabled = false;
            _moveScript.enabled = false;
            _cinematic = true;
            _fog.SetActive(true);
        }
    }
}
