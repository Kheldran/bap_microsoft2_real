﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour {

    public int _boolNumber;
    public GameObject[] _Statues;
    public bool[] _areStatueFinished;
    private bool _statueCheck;

    private bool _allStatueFinished;
    private int _cameraNumber;
    private Animator _doorOpenAnim;

	// Use this for initialization
	void Start () {
        _doorOpenAnim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

        Debug.Log("statue" + _boolNumber + "=" + _areStatueFinished[_boolNumber]);
        _allStatueFinished = true;          //permet d'ouvrir la porte si tout les bools dans la list _areStatueFinished sont true.


        for (int i = 0; i < _Statues.Length; ++i)                               // pour chaque GameObject de la liste _Statues, check dans le script "Statue" si le timer d'éclairage de la statue est fini, si oui rend un bool de la liste des bools true
        {

            _statueCheck = _Statues[i].GetComponent<Statue>()._wasStatueFinished;
            if (_statueCheck)
            {
                _boolNumber = i;
                _areStatueFinished[_boolNumber] = true;
               
            }

        }
    
        for (int i = 0; i < _areStatueFinished.Length; ++i)         // tant que un des éléments de la list des bools est faux, rend _allStatueFinished faux.
        {
            if (_areStatueFinished[i] == false)
            {
                _allStatueFinished = false;
            }
        }



        if  (_allStatueFinished)                                    // si tout les éléments de la liste bool sont vraies, ouvre la porte.
        {
            _doorOpenAnim.enabled = true;
            this.GetComponent<BoxCollider2D>().enabled = false;
        }


}
}
