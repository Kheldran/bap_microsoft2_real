﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Statue : MonoBehaviour
{

    // public GameObject _wall;
    // public GameObject _wallDisplaced;

    private float _lanternTimer;
    public bool _wasStatueFinished { get; private set; }
    private bool _timerReset;
    private float _timerCheckpoint;

    private GameObject _player;
    private Move _moveScript;

    private bool audioStatueOneTime = false;

    public Sprite[] statueGaugeTab;
    private float palier;
    private int i = 24;
    private int iMax = 24;
    private float numberOfSprites = 24;
    // Use this for initialization
    void Start()
    {
        palier = 3 * (24 / 24);
        _lanternTimer = 3;
        _timerCheckpoint = 3;
        _player = GameObject.FindWithTag("Player");
        _moveScript = _player.GetComponent<Move>();

    }

    // Update is called once per frame
    void Update()
    {
        // make it so that the sprite of the statue changes according to the lantern filling up the statue

        if (_lanternTimer <= palier)
        {
            i--;
            numberOfSprites--;
            palier = 3 * (numberOfSprites / 24);
            
        }
        else if (_timerReset)
        {
            i++;
            numberOfSprites++;
            if (i > iMax)
            {
                i = iMax;
            }
            if (numberOfSprites > iMax)
            {
                numberOfSprites = iMax;
            }
        }
        if (_lanternTimer > 0 && i != 0) { 
            this.GetComponent<SpriteRenderer>().sprite = statueGaugeTab[i - 1];
        }
        Debug.Log(_lanternTimer + "checkpoint " + _timerCheckpoint);

        if (_timerReset)            // si on sort la lanterne du collider de la statue, reset le timer
        {
            _lanternTimer = _timerCheckpoint;
            if (_lanternTimer < _timerCheckpoint)
            {
                _lanternTimer += 1f * Time.deltaTime;
            }
            _timerReset = false;
        }

        if (_lanternTimer <= 0)       // si on n'éclaire plus la statue et que le timer était inférieur ou égale à 0, le rend égale à 0
        {
            _lanternTimer = 0;
            _wasStatueFinished = true;
            
            if (!audioStatueOneTime)
            {
                AkSoundEngine.PostEvent("Statue_Event", gameObject);
                audioStatueOneTime = true;
            }
            this.GetComponent<SpriteRenderer>().sprite = statueGaugeTab[0];
        }

        if (_lanternTimer <= 1.5f)
        {
            _timerCheckpoint = 1.5f;
            iMax = 12;
        }

        if (_lanternTimer > 0 && !_moveScript._lanternOn)       // si on éteint la lanterne alors qu'on éclairait une statue, reset le timer
        {
            _timerReset = true;
        }

        /* if (_wasStatueFinished)                                                                                 // si exit et le timer était égal à 0
         {
             Vector3 position = _wall.transform.position;                                                        // fait baisser le wall
             position -= new Vector3(0, 0, -0.02f);
             _wall.transform.position = position;

             if (_wall.transform.position.z >= _wallDisplaced.transform.position.z)                              // si la position du wall en z est inférieur ou égale à la position finale (tranform du public gameObject wallDisplaced) du wall en z
             {
                 _wall.transform.position = _wall.transform.position;                                        // rend les positions égales
                 _lanternTimer = 3;                                                                              // réinitialise le timer
                 _wasStatueFinished = false;
                 _wall.GetComponent<Collider2D>().enabled = false;
             }
         }*/
    }


    void OnTriggerStay2D(Collider2D other)
    {


        if (other.CompareTag("lanterne"))       // si other == lanterne
        {
            _lanternTimer -= 1f * Time.deltaTime;            // fait baisser le timer du wall
            /*    if (_lanternTimer <= 0 && _wall.transform.position.z <= _wallDisplaced.transform.position.z)        // si le timer du wall inférieur ou égal à 0 et que la position du wall est différente de sa position finale en z
           {
           Vector3 position = _wall.transform.position;                                                        // fait baisser le wall
           position -= new Vector3(0, 0, -0.02f);
            _wall.transform.position = position;
           _lanternTimer = 0;                                                                              // met le timer à 0  
           }

            if (_wall.transform.position.z >= _wallDisplaced.transform.position.z)                              // si la position du wall en z est inférieur ou égale à la position finale du wall en z
           {
           _wall.transform.position = _wall.transform.position;                                        // rend les positions égales
           _lanternTimer = 3;                                                                              // réinitialise le timer
           _wall.GetComponent<Collider2D>().enabled = false;                                           //désactive le collider qui nous empechait de rentrer dans la pièce apres la porte
           }*/
        }

    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (_lanternTimer > 0)                                                                                 // si on n'éclaire plus la statue avant que le timer ne soit fini, reset le timer
        {
            _timerReset = true;
        }



    }
}
