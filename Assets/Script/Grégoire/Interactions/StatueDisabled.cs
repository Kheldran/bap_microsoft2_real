﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatueDisabled : MonoBehaviour {

    public GameObject _player;
    private RamasserObjetsStatues _ramasserObjetsStatuesScript;
    private Statue _statueScript;


	// Use this for initialization
	void Start () {
        //get les scripts dont on a besoin
        _statueScript = this.gameObject.GetComponent<Statue>();
        _ramasserObjetsStatuesScript = _player.gameObject.GetComponent<RamasserObjetsStatues>();
        //rend la statue inutilisable tant qu'on a pas réparer la glyphe
        _statueScript.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {

        if (_ramasserObjetsStatuesScript._statueActivation)
        {
            //si la glyphe est réparée, rend la statue utilisable
            _statueScript.enabled = true;
        }


	}
}
