﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleActivation : MonoBehaviour {


    public GameObject statue;
    public GameObject statueRight;

    private TargetIndicator targetIndicatorScriptLeft;
    private TargetIndicator targetIndicatorScriptRight;

    private TargetIndicatorTimer targetIndicatorTimerScriptLeft;
    private TargetIndicatorTimer targetIndicatorTimerScriptRight;

	// Use this for initialization
	void Start () {
        targetIndicatorScriptLeft = statue.GetComponent<TargetIndicator>();
        targetIndicatorTimerScriptLeft = statue.GetComponent<TargetIndicatorTimer>();
        targetIndicatorScriptLeft.enabled = false;
        targetIndicatorTimerScriptLeft.enabled = false;

        targetIndicatorTimerScriptRight = statueRight.GetComponent<TargetIndicatorTimer>();
        targetIndicatorScriptRight = statueRight.GetComponent<TargetIndicator>();
        targetIndicatorScriptRight.enabled = false;
        targetIndicatorTimerScriptRight.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	}

     void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && this.CompareTag("GlypheLeft"))
        {
            targetIndicatorScriptLeft.enabled = true;
            targetIndicatorTimerScriptLeft.enabled = true;
            targetIndicatorTimerScriptLeft.timer = targetIndicatorTimerScriptLeft._indicatorTime;
            if (targetIndicatorScriptLeft.m_iconImage != null)
            {
                targetIndicatorScriptLeft.m_iconImage.gameObject.SetActive(true);
            }
            if (targetIndicatorScriptRight.m_iconImage != null)
            {
                targetIndicatorScriptRight.m_iconImage.gameObject.SetActive(false);
            }
            targetIndicatorScriptRight.enabled = false;
            targetIndicatorTimerScriptRight.enabled = false;
        }
        else if (other.CompareTag("Player") && this.CompareTag("GlypheRight"))
        {
            targetIndicatorScriptRight.enabled = true;
            targetIndicatorTimerScriptRight.enabled = true;
            targetIndicatorTimerScriptRight.timer = targetIndicatorTimerScriptRight._indicatorTime;
            if (targetIndicatorScriptRight.m_iconImage != null)
            {
                targetIndicatorScriptRight.m_iconImage.gameObject.SetActive(true);
            }
            targetIndicatorScriptLeft.enabled = false;
            targetIndicatorTimerScriptLeft.enabled = false;
            if (targetIndicatorScriptLeft.m_iconImage != null)
            {
                targetIndicatorScriptLeft.m_iconImage.gameObject.SetActive(false);
            }

        }
        /* //active les particule quand on passe sur la glyphe
    if (other.CompareTag("Player"))
    {
        transform.GetChild(0).gameObject.SetActive(true);
    }*/
    }

    void OnTriggerExit2D(Collider2D other)
    {

        //désactive les particule quand on sort de la glyphe
        /* if (other.CompareTag("Player"))
         {
             transform.GetChild(0).gameObject.SetActive(false);
         }*/
    }
}
