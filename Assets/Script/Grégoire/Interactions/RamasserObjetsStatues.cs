﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RamasserObjetsStatues : MonoBehaviour {

    public bool _circle;
    public bool _square;
    public bool _triangle;
    private bool _circleActivated;
    private bool _squareActivated;
    private bool _triangleActivated;
    private bool _canIPickUpTriangle;
    private bool _canIPickUpCircle;
    private bool _canIPickUpSquare;

    public bool _statueActivation;
    private bool _canIPickUpSomething;
    public Text _quoteToPickup;

    public GameObject glyphe;
    private SpriteRenderer glypheSR;
    public Sprite _noObject;
    public Sprite _allObject;
    public Sprite _squareFill;
    public Sprite _squareCircleFill;
    public Sprite _squareTriangleFill;
    public Sprite _circleFill;
    public Sprite _circleTriangleFill;
    public Sprite _triangleFill;

    // Use this for initialization
    void Start () {

       glypheSR = glyphe.GetComponent<SpriteRenderer>();
        _canIPickUpCircle = false;
        _canIPickUpTriangle = false;
        _canIPickUpSquare = false;
    }
	
	// Update is called once per frame
	void Update () {
        //si la glyphe est entièrement réparée, permet de réutiliser la statue
        if (_circleActivated && _squareActivated && _triangleActivated)
        {
            _statueActivation = true;
        }

        if (_canIPickUpTriangle && Input.GetKeyDown("joystick button 1"))
        {
            GameObject.FindGameObjectWithTag("triangle").SetActive(false);
            _triangle = true;
            _quoteToPickup.enabled = false;
            _canIPickUpTriangle = false;
        }

        if (_canIPickUpCircle && Input.GetKeyDown("joystick button 1"))
        {
            GameObject.FindGameObjectWithTag("circle").SetActive(false);
            _circle = true;
            _quoteToPickup.enabled = false;
            _canIPickUpCircle = false;
        }

        if (_canIPickUpSquare && Input.GetKeyDown("joystick button 1"))
        {
            GameObject.FindGameObjectWithTag("square").SetActive(false);
            _square = true;
            _quoteToPickup.enabled = false;
            _canIPickUpSquare = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("circle"))
        {
            _canIPickUpCircle = true;
            _quoteToPickup.enabled = true;
        }
        else if (other.CompareTag("square"))
        {
            _canIPickUpSquare = true;
            _quoteToPickup.enabled = true;
        }
        else if (other.CompareTag("triangle"))
        {
            _canIPickUpTriangle = true;
            _quoteToPickup.enabled = true;
        }


    }

        private void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("statueActivation"))
        {
            if (_circle || _triangle || _square)
            {
                _quoteToPickup.enabled = true;
                _quoteToPickup.text = "Appuyer sur B pour réparer la glyphe";
            }
        }
        //si on est sur la glyphe et qu'on appuie sur A
        if (other.CompareTag("statueActivation") && Input.GetKeyDown("joystick button 1"))
        {
            if (_circle || _triangle || _square)
            {
                _quoteToPickup.text = "Appuyer sur B pour réparer la glyphe";
            }
            // set l'apparence de la glyphe en fonction des parties qu'on a déjà récolté
            if (_square && _circle && _triangle)
            {
                glypheSR.sprite = _allObject;
                _squareActivated = true;
                _circleActivated = true;
                _triangleActivated = true;
            }
            if (_square && _circle)
            {
                glypheSR.sprite = _squareCircleFill;
                _squareActivated = true;
                _circleActivated = true;
            }
            if (_square && _triangle)
            {
                glypheSR.sprite = _squareTriangleFill;
                _triangleActivated = true;
                _squareActivated = true;
            }
            if (_circle && _triangle)
            {
                glypheSR.sprite = _circleTriangleFill;
                _triangleActivated = true;
                _circleActivated = true;
            }
            if (_circle)
            {
                glypheSR.sprite = _circleFill;
                _circleActivated = true;
            }
            if (_square)
            {
                glypheSR.sprite = _squareFill;
                _squareActivated = true;
            }
            if (_triangle)
            {
                glypheSR.sprite = _triangleFill;
                _triangleActivated = true;
            }
            if (!_triangle && !_circle && !_square)
            {
                glypheSR.sprite = _noObject;
            }



        }
        //si le tag de l'objet est cercle, si on appuie sur B, fait en sorte que l'on puisse réparer la gluphe cercle
        /*if (other.CompareTag("circle"))
        {

            if (Input.GetKeyDown("joystick button 1") && _canIPickUpCircle)
            {

                other.gameObject.SetActive(false);
                _circle = true;
                _quoteToPickup.enabled = false;
            }
        }
        //same
        if (other.CompareTag("square") && _canIPickUpSquare)
        {

            if (Input.GetKeyDown("joystick button 1"))
            {
                other.gameObject.SetActive(false);
                _square = true;
                _quoteToPickup.enabled = false;
            }
        }
        //same
        if (other.CompareTag("triangle") && _canIPickUpTriangle)
        {

            if (Input.GetKeyDown("joystick button 1"))
            {
                other.gameObject.SetActive(false);
                _triangle = true;
                _quoteToPickup.enabled = false;
            }
        }*/

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("circle"))
        {
            _quoteToPickup.enabled = false;
            _canIPickUpCircle = false;
        }
        if (other.CompareTag("square"))
        {
            _quoteToPickup.enabled = false;
            _canIPickUpSquare = true;
        }
        if (other.CompareTag("triangle"))
        {
            _quoteToPickup.enabled = false;
            _canIPickUpTriangle = true;
        }

        if (other.CompareTag("statueActivation"))
        {
            _quoteToPickup.enabled = false;
            _quoteToPickup.text = "Appuyer sur B pour ramasser l'objet";
        }
    }
}
