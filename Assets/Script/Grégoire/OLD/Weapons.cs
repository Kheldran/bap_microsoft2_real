﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapons : MonoBehaviour {

	public Transform _weaponPlacement;		//permet de positionner l'arme devant le joueur

	public bool _weapon;					//permet de verifier si l'on peut changer d'arme
	public bool _firstWeapon;				//permet de ramasser la premiere arme du jeu.
	public bool _timerBegin;				//permet de cmmencer le timer pour pouvori ramasser une nouvelle arme
	public bool _isWeaponEquiped = false;	//permet de reconnaitre si l'on a une arme d'équipée


	public float _changeRate = 2.0f;		//temps qu'il faudra attendre pour pouvoir ramasser une nouvelle arme.
	public float _time = 0.0f;				//temps initial.
	private Move _moveScript;				//se rattache au scirpt Move.



	// Use this for initialization
	void Start () {
		_moveScript = this.GetComponent<Move> ();
	}
	
	// Update is called once per frame
	void Update () {



		if (_timerBegin) {
			
			_time += Time.deltaTime;		// commence le timer pour le changement d'arme sur le sol.

			if (_time >= _changeRate) {
											
				_weapon = true;				// fait en sorte que le triggerEnter puisse se répéter
				_timerBegin = false;
				_time = 0;
			}
		}
	}

	public void OnTriggerEnter2D(Collider2D other) {

		if (_weapon && other.tag == "weapons") {
			GameObject _equiped = GameObject.FindWithTag ("equipedWeapon");			//trouve le gameObject de l'arme en cours d'utilisation
			Vector3 _floorPosition = other.transform.position;						// prends la position de l'arme que l'on veut ramasser
			_equiped.transform.position = _floorPosition;							// donne à l'arme en cours d'utilisation la position de l'arme que l'on veut ramasser
			_equiped.transform.parent = null;										// enleve le parent de l'arme en cours d'utilisation
			_equiped.gameObject.tag = "weapons";									// lui redonne le tag "weapons" pour qu'on puisse la ramasser si l'on veut apres


			other.transform.position = _weaponPlacement.position;					// fait en sorte que la position de l'arme par terre se mette a la place d'un empty devant la main du sprite joueur
			other.transform.rotation = _weaponPlacement.rotation;				
			other.transform.parent = this.gameObject.transform;						// fait en sorte que le parent de l'arme ramassé soit le joueur
			other.gameObject.tag = "equipedWeapon";									// donnes le tag 'equipedWeapon' à l'arme ramassée


			_weapon = false;														//fais en sorte que cette fonction ne se repete pas à l'infini
			_timerBegin = true;														// permet de mettre en amrche le timer pour pouvoir répéter cette fonction apres quelques secondes.
			_moveScript._firstShoot = true;											// permet de tirer la premiere balle sans problème apres un changement d'arme
			_isWeaponEquiped = true;												// Verifie qu'il y a bien une arme d'équipée.
		}
	
		if (other.tag == "weapons" && _firstWeapon == false) {
		
			other.transform.position = _weaponPlacement.position; 
			other.transform.rotation = _weaponPlacement.rotation;		// fait en sorte que la position de l'arme par terre se mette a la place d'un empty devant la main du sprite joueur
			other.transform.parent = this.gameObject.transform;			// fait en sorte que le parent de l'arme ramassé soit le joueur
			other.gameObject.tag = "equipedWeapon";						// donnes le tag 'equipedWeapon' à l'arme ramassée
			_weapon = false;											// permet de faire fonctionner la fonction de remplacement de l'arme
			_firstWeapon = true;										// fait en sorte que cette fonction ne soit plus utilisée.
			_timerBegin = true;											// enclenche le timer avant de pouvoir ramasser une arme a nouveau
			_isWeaponEquiped = true;									// Verifie qu'il y a bien une arme d'équipée.
		}

	
	
	
	
	}
}
