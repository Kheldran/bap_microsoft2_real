﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotsMovement : MonoBehaviour {

	public float _bulletSpeed = 15f;		//influe sur la vitesse de la balle.

	private Enemy _enemyScript;				// script ennemy
	private Shoot _shootScript;				// script shoot
	private GameObject _firearm;			//arme équipée
	// Use this for initialization
	void Start () {
		_bulletSpeed = 15f;												// Vitesse de la balle
		_firearm = GameObject.FindGameObjectWithTag ("equipedWeapon");	// trouve l'objet équipé
		_shootScript = _firearm.GetComponent<Shoot> (); 				// Prends le script shoot de l'arme équipé
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += transform.forward * Time.deltaTime * _bulletSpeed;		//fait en sorte que la balle se dirige tout droit la on l'on veut tirer.


	
	}


	void OnTriggerEnter2D(Collider2D other) {
	
		if (other.gameObject.tag == "boundary") {

			Destroy (this.gameObject);			//détruit la balle si elle touche un mur.
	
		}

		if (other.gameObject.tag == "enemy") {
		
			_enemyScript = other.GetComponent<Enemy> ();	//prends le script Enemy
			if (_enemyScript.invulnerability == false) {	// si l'enemy n'est pas invulnérable 

				_enemyScript._enemyLife -= _shootScript._weaponDamage;					//enleve de la vie a l'ennemy en fonction des dommages de l'arme
				_enemyScript.invincibilityCounter = _enemyScript.invincibilityLength;	// rends l'ennemy invulnérable pdt le temsp de invincibilityLength
				_enemyScript._enemyRender.enabled = false; 								// fait en sorte que le render de l'ennemi se désactive
				_enemyScript.flashCounter = _enemyScript.flashLength;					// active l'intervalle de temps que met le render pour se réactiver/désactiver
				_enemyScript.invulnerability = true;									// fait en sorte que le script d'invulnérabilité commence
                Destroy(gameObject);
			}
		}

	}
}
