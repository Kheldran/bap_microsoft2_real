﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour {

	public GameObject _shoot;		//permet d'appeller le gameobject du tir de l'arme.
	public float _fireRate = 1f;	//vitesse de tir de l'arme
	public float _fireReload = 0f;	//vitesse de rechargement du tir.
	public int _weaponDamage;		//dommages de l'arme

	// Use this for initialization
	void Start () {
		// stats du premier gun
		if (this.gameObject.name == "Gun1") {
			_weaponDamage = 1; 		// dommages de l'arme	
			_fireRate = 0.5f;		// vitesse de tir
		}

		// stats du 2eme gun
		if (this.gameObject.name == "Gun2") {
			_weaponDamage = 2;
			_fireRate = 1f;
		}
	}
	
	// Update is called once per frame
	void Update () {

	}
}
