﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	public int _enemyLife;					// vie de l'ennemie
	private int _lifeDown;					// agit quand la vie de l'ennemi descend

	public SpriteRenderer _enemyRender;			// Renderer de l'ennemy

	public float invincibilityLength;		// temps d'invincibilité
	public float invincibilityCounter;		// temps d'invincibilité qui s'écoule
	public float flashCounter = 0;			// Réactive/désactive le renderer
	public float flashLength = 0.1f;		// temps avant que l'on désactive/réactive le renderer
	public bool invulnerability = false;	// vérifie l'invulnérabilité

    private float _hitTimer;
    private bool _hit;

    private Rigidbody2D _rb;



	// Use this for initialization
	void Start () {

        _rb = this.GetComponent<Rigidbody2D>();


        if (this.gameObject.name == "Enemy1") {     //attention, la vie des ennemis est codée grâce à leur noms, si ils n'ont pas un des noms listés ici, ils meurent directement
			//vie de l'ennemy1
			_enemyLife = 5;
		
		}

		if (this.gameObject.name == "Enemy2") {
			//vie de l'ennemy2
			_enemyLife = 3;
		
		}
		_lifeDown = _enemyLife;								// garde l'ancienne valeur de vie
		_enemyRender = this.GetComponent<SpriteRenderer> ();	//prends le composant renderer de ennemy
		invincibilityLength = 0.3f;						// fais en sorte que l'invincibilité dure 0.3 secondes
        _hitTimer = 0.3f;
	}
	
	// Update is called once per frame
	void Update () {

      /*  if (_hit)   //crée un countdown pour faire perdre des pvs aux enemis toute les 0.3 secondes si ils sont dans le trigger de la lanterne.
        {
            _hitTimer -= Time.deltaTime;
            if (_hitTimer <= 0)
            {
                _hit = false;
                _hitTimer = 0.3f;
            }
        }

		if (_enemyLife <= _lifeDown) {								// si il y a perte de vie

			if (invincibilityCounter > 0) {							

				invincibilityCounter -= Time.deltaTime;				//fais s'écouler la période d'invulnérabilité
				flashCounter -= Time.deltaTime;						// fais s'écouler la période d'activation/désactivation du renderer

				if (flashCounter <= 0) {							// si la période d'activation/désactivation du renderer <=0

					_enemyRender.enabled = !_enemyRender.enabled;	// désactive/active le renderer
					flashCounter = flashLength;						//reset la période d'activation/désactivation du renderer
				}

				if (invincibilityCounter <= 0) {					// si la durée d'invincibilité est écoulée

					_enemyRender.enabled = true;					// active le renderer
					invulnerability=false;							// permet a l'ennemy de se reprendre des dommages, réactive la possiblité du trigger dans le script ShotsMovement

				}	
			}
			_lifeDown = _enemyLife;									//garde l'ancienne valeur de vie
		
		}

		if (_enemyLife <= 0) {
		
			Destroy (this.gameObject);							// si vie = 0, détruit l'ennemy
		
		}*/


	}



}
