﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bouton : MonoBehaviour {

    public GameObject _gameManager;
    private Day_Night _dayNightScript;
    
	// Use this for initialization
	void Start () {
        _dayNightScript = _gameManager.GetComponent<Day_Night>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D other) {               // repasse au jour si le joueur passe sur le bouton
        if (other.tag == "Player" && _dayNightScript._isDayOn == false) {

            _dayNightScript._isDayOn = true;
        }
    }
}
