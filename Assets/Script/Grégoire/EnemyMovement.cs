﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

    private int layerMask;
    private float _latestTimeChange;
    private int _itsChangingTime;
    private float _speed;

    private float _stoneRecoveryTimer;
    private int _stoneRecoveryTimerMax;

    private Vector2 _changingDirection;
    private Vector2 _actualMovement;

    public GameObject _playerPosition;
    public float _moveSpeed;     // vitesse de déplacement du mob quand aggro
    private int _maxAggroRange = 30; //distance max d'aggro;

    private bool _isPlayerSighted;
    private bool _wall;
    public bool _amIStoned {get; private set;}
    private bool _amIBeingStoned;
    private bool _isWallBeforeMe;


    private bool _rightTurn;
    private bool _leftTurn;
    private bool _hasTurned;
    private bool _wallCheck;
    private bool _wallCheckEnd;


    private float _stoneTimer;

    public Sprite _notStoned;
    public Sprite _stoneStage1;
    public Sprite _stoneStage2;
    public Sprite _stoned;

    private Move _moveScript;
    private GameObject _gameController;
    private Day_Night _dayNightScript;

    public float raycastDistance;
    private float timer = 0.3f;
    private float timer2 = 0.3f;

    // Use this for initialization
    void Start () {

        layerMask = LayerMask.GetMask("Default");
        _stoneRecoveryTimerMax = 5; //temps d'immobilisation des monstres

        _latestTimeChange = 0f;     //initialise le timer de changement de direction
        _itsChangingTime = 3;      // temps avant le prochain changement de direction du mob
        _speed = 3f;   // Vitesse de déplacement du mob sans aggro

        _gameController = GameObject.FindWithTag("GameController");

        _moveScript = _playerPosition.GetComponent<Move>();     // accède au script du personnage joueur pour savoir si il est dans une zone feu de camp ou non
        _dayNightScript = _gameController.GetComponent<Day_Night>();

        raycastDistance = 5;
        
        
    }

    private void FixedUpdate()
    {
        // Save current object layer
        int oldLayer = gameObject.layer;

        //Change object layer to a layer it will be alone
        gameObject.layer = LayerMask.NameToLayer("Sol");

        int layerToIgnore = 1 << gameObject.layer;
        layerToIgnore = ~layerToIgnore;



        // Cast a ray straight down.
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.up, raycastDistance, layerToIgnore);
        RaycastHit2D hitUpLeft = Physics2D.Raycast(transform.position, -transform.right  + transform.up, raycastDistance, layerToIgnore);
        RaycastHit2D hitUpRight = Physics2D.Raycast(transform.position, transform.right  + transform.up, raycastDistance, layerToIgnore);
        RaycastHit2D hitRight = Physics2D.Raycast(transform.position, transform.right, raycastDistance + 100, layerToIgnore);
        RaycastHit2D hitLeft = Physics2D.Raycast(transform.position, -transform.right, raycastDistance + 100, layerToIgnore);

        Debug.DrawRay(this.transform.position, (-transform.right  + transform.up) * raycastDistance);
        Debug.DrawRay(this.transform.position, (transform.right + transform.up) * raycastDistance);
        Debug.DrawRay(this.transform.position, transform.up * raycastDistance);
        Debug.DrawRay(this.transform.position, transform.right * (raycastDistance + 100));
        Debug.DrawRay(this.transform.position, -transform.right * (raycastDistance + 100));

        // If it hits something...
        if (hit.collider != null && !_isWallBeforeMe)
        {
           

            if (hit.collider.transform.tag != "Player" && _isPlayerSighted && !_isWallBeforeMe)
            {
                Debug.Log("hit");
                _isWallBeforeMe = true;

            }

            if (hit.collider.transform.tag == "boundary")
            {
                
            }
        }
        else if (hitUpRight.collider != null && !_isWallBeforeMe)
        {
            Debug.Log("hitUpRight");

            if (hitUpRight.collider.transform.tag != "Player" && _isPlayerSighted && !_isWallBeforeMe)
            {
                _isWallBeforeMe = true;

            }
            if (hitUpRight.collider.transform.tag == "boundary")
            {
                transform.Rotate(this.transform.eulerAngles.x, this.transform.eulerAngles.y, this.transform.eulerAngles.z + 270);
            }
        }
        else if (hitUpLeft.collider != null && !_isWallBeforeMe)
        {
            Debug.Log("hitUpleft");

            if (hitUpLeft.collider.transform.tag != "Player" && _isPlayerSighted && !_isWallBeforeMe)
            {
                _isWallBeforeMe = true;

            }
            if (hitUpLeft.collider.transform.tag == "boundary")
            {
                transform.Rotate(this.transform.eulerAngles.x, this.transform.eulerAngles.y, this.transform.eulerAngles.z + 180);
            }
        }

        if (_isWallBeforeMe)
        {
            if (hitRight.distance < hitLeft.distance && !_amIStoned)
            {
                print("hitleft");
                if (!_hasTurned)
                {
                    transform.Rotate(this.transform.eulerAngles.x, this.transform.eulerAngles.y, this.transform.eulerAngles.z + 30);
                    //this.transform.rotation = Quaternion.Lerp(this.transform.rotation, new Quaternion(this.transform.rotation.x, this.transform.rotation.y, this.transform.rotation.z + 90, this.transform.rotation.w), Time.deltaTime *500);
                    //this.transform.rotation = new Quaternion(this.transform.rotation.x, this.transform.rotation.y, this.transform.rotation.z + 90, this.transform.rotation.w);
                    _hasTurned = true;
                    
                }
                _isPlayerSighted = false;
                if (hitRight.distance < raycastDistance && _hasTurned)
                {

                    transform.position += transform.up * _moveSpeed * Time.deltaTime;

                }
                else if (hitRight.distance > raycastDistance && _hasTurned)
                {
                    _wallCheck = true;
                    if (!_wallCheckEnd)
                    {
                        transform.position += transform.up * _moveSpeed * Time.deltaTime;
                    }
                    else if (_wallCheckEnd)
                    {
                        _isWallBeforeMe = false;
                        _wallCheckEnd = false;
                    }

                    Debug.Log(_isWallBeforeMe);
                }
                // _leftTurn = true;
                // _rightTurn = false;

            }
            else if (hitLeft.distance <= hitRight.distance && !_amIStoned)
            {
                print("hitright");
                if (!_hasTurned)
                {
                    transform.Rotate(this.transform.eulerAngles.x, this.transform.eulerAngles.y, this.transform.eulerAngles.z - 30);
                    //this.transform.rotation = Quaternion.Lerp(this.transform.rotation, new Quaternion(this.transform.rotation.x, this.transform.rotation.y, this.transform.rotation.z - 90, this.transform.rotation.w), Time.deltaTime * 500);
                    //this.transform.rotation = new Quaternion(this.transform.rotation.x, this.transform.rotation.y, this.transform.rotation.z -90, this.transform.rotation.w);
                    _hasTurned = true;
                }
                if (hitRight.distance < raycastDistance && _hasTurned)
                {
                    transform.position += transform.up * _moveSpeed * Time.deltaTime;

                }
                else if (hitRight.distance > raycastDistance  && _hasTurned)
                {
                    _wallCheck = true;
                    if (!_wallCheckEnd)
                    {
                        transform.position += transform.up * _moveSpeed * Time.deltaTime;
                    }
                    else if (_wallCheckEnd)
                    {
                        _isWallBeforeMe = false;
                        _wallCheckEnd = false;
                        timer2 = 0.3f;
                    }
                    

                }
                // _rightTurn = true;
                //_leftTurn = false;
                _isPlayerSighted = false;
            }

            //if (_rightTurn)
            //{
            //    if (hitLeft.distance > raycastDistance + 2)
            //    {
            //        _isWallBeforeMe = false;
            //        _hasTurned = false;

            //    }
            //}
            //if (_leftTurn)
            //{


            //    timer -= Time.deltaTime;
            //    Debug.Log(timer);
            //    if (hitRight.distance > raycastDistance)
            //    {
            //        _isWallBeforeMe = false;
            //        _hasTurned = false;
            //        timer = 0;
            //    }
            //    else if (timer <0)
            //    {
            //        timer = 3;
            //        _hasTurned = false;
            //    }
            //}
            // set the game object back to its original layer
            gameObject.layer = oldLayer;
        }

    }

    // Update is called once per frame
    void Update () {

        if (_wallCheck)
        {
            timer2 -= Time.deltaTime;
            if (timer2 <= 0)
            {
                _wallCheckEnd = true;
                _wallCheck = false;


            }
        }


        if(!_isWallBeforeMe)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                _hasTurned = false;
                timer = 0.2f;
            }
        }





        if (Vector3.Distance(_playerPosition.transform.position, transform.position) <= _maxAggroRange && !_moveScript._isPlayerInFire && !_amIStoned && !_isWallBeforeMe)   // si la distance entre le deux objets est inférieure ou égale à maxAggroRange ou si le joueur n'est pas dans le feu et que le monstre n'est pas stun et qu'il fait nuit
        {
            //fait en sorte que le mob se tourne vers le joueur 
            Vector3 diff = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            diff.Normalize();

            float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);

            transform.position += transform.up * _moveSpeed * Time.deltaTime;      // fait en sorte que le mob suive le joueur 
            _isPlayerSighted = true;                                                // si le joueur est dans la range d'aggro du mob, enlève les déplacement aléatoire 
            _latestTimeChange = 3f;
        }

        if (Vector3.Distance(_playerPosition.transform.position, transform.position) >= _maxAggroRange || _moveScript._isPlayerInFire && !_amIStoned )
        {
            _isPlayerSighted = false;           // si le joueur est au-delà de la limite d'aggro du monstre, lui permet de se déplacer aléatoirement
            
        }

        if (!_isPlayerSighted && !_amIStoned && !_isWallBeforeMe)
        {
            _latestTimeChange += Time.deltaTime;
            if (_latestTimeChange > _itsChangingTime)   // si _itsChangingTime secondes se sont écoulées
            {
                _latestTimeChange = 0f;                      // réinitialise le temps de _latestTimeChange
                CalculateNewDirection();                            // calcule et actualise la nouvelle direction du mob
                _wall = false;
            }

            if (!_wall) // si le monstre ne rencontre pas d'obstacle, il continue a se déplacer aléatoirement
            {
                transform.position = new Vector2(transform.position.x + (_actualMovement.x * Time.deltaTime), transform.position.y + (_actualMovement.y * Time.deltaTime));     //déplacement du mob sans aggro
            }
            if (_wall)// si le monstre rencontre des obstacles, il recule.
            {
                //this.transform.rotation = Quaternion.Lerp(this.transform.rotation, new Quaternion(this.transform.rotation.x, this.transform.rotation.y, this.transform.rotation.z - 90, this.transform.rotation.w), Time.deltaTime);
                transform.position = new Vector2(transform.position.x + (-_actualMovement.x * Time.deltaTime) , transform.position.y + (-_actualMovement.y * Time.deltaTime));     //fait reculer le mob quand il percute un mur
            }
            
        }

        if (_amIBeingStoned )
        {
            if (_moveScript._lanternOn)
            { 
                 _stoneTimer += Time.deltaTime;          //augmente le timer de pétrification
            }
            if (_stoneTimer > 0 && !_moveScript._lanternOn)
            {
                _amIBeingStoned = false;
            }
            if (_stoneTimer < 0.65f)                 // change le sprite en non pétrifié quand le timer est à moins de 0.5s
            {
                this.GetComponent<SpriteRenderer>().sprite = _notStoned;
            }
            if (_stoneTimer >=0.65f && _stoneTimer < 1.30f)         // change le sprite en pétrification stage 1 quand le timer est à moins de 1s
            {
                this.GetComponent<SpriteRenderer>().sprite = _stoneStage1;
            }
            if (_stoneTimer >= 1.30f && _stoneTimer < 2f)        // change le sprite en pétrification stage 2 quand le timer est à moins de 1.5s
            {
                this.GetComponent<SpriteRenderer>().sprite = _stoneStage2;
            }
            if (_stoneTimer >= 2f)                   //si l'ennemi reste dans la lanterne pdt 2 secs le pétrifit
            {
                _amIStoned = true;                  //lance le timer de dépétrification
                this.GetComponent<SpriteRenderer>().sprite = _stoned;
                _stoneTimer = 2.5f;
            }
        } else
        {
            if (_stoneTimer > 0)
            {
                 _stoneTimer -= 0.5f * Time.deltaTime;
            }
            if (_stoneTimer < 0.65f)                 // change le sprite en non pétrifié quand le timer est à moins de 0.5s
            {
                this.GetComponent<SpriteRenderer>().sprite = _notStoned;
            }
            if (_stoneTimer >= 0.65f && _stoneTimer < 1.30f)         // change le sprite en pétrification stage 1 quand le timer est à moins de 1s
            {
                this.GetComponent<SpriteRenderer>().sprite = _stoneStage1;
            }
            if (_stoneTimer >= 1.30f && _stoneTimer < 2f)        // change le sprite en pétrification stage 2 quand le timer est à moins de 1.5s
            {
                this.GetComponent<SpriteRenderer>().sprite = _stoneStage2;
            }
            if (_stoneTimer >= 2f)                   //si l'ennemi reste dans la lanterne pdt 2 secs le pétrifit
            {
                this.GetComponent<SpriteRenderer>().sprite = _stoned;
            }
        }

        if (_amIStoned)         //si le monstre est stun
        {

            //_stoneRecoveryTimer += Time.deltaTime;      //débute le timer de recovery du stun
            _amIBeingStoned = false;
            if (_stoneTimer <= 0)
            {
                _amIStoned = false;             // si le timer est fini enlève le stun
               // _stoneRecoveryTimer = 0f;               //reset le timer pour le prochain stun
                _stoneTimer = 0f;           //reset le timer pour le temps de pétrification
                this.GetComponent<SpriteRenderer>().sprite = _notStoned;
            }

        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("feu"))
        {
            _wall = true;       // si le monstre rentre dans la zone du feu de camp, il recule
        }
        

    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("lanterne"))           // si l'ennemi entre dans la lumière de la lanterne, le stun     
        {
            _amIBeingStoned = true;                 //lance le timer de pétrification
        }

        if (other.CompareTag("feu"))
        {
            _wall = true;       // si le monstre rentre dans la zone du feu de camp, il recule
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("lanterne"))           // si l'ennemi sort de lanterne, diminue le compteur de pétrification   
        {
            _amIBeingStoned = false;                //ne fait pas tourner le timer pour la pétrification quand la lanterne sort du trigger de l'ennemi

        }
        if (other.CompareTag("boundary"))       // si le monstre rentre dans le trigger "boundary', il recule
        {
            _wall = true;
        }
    }


    void OnCollisionEnter2D(Collision2D other)     
    {

        if (other.gameObject.tag != "Player")       // si le monstre rencontre des obstacles autres que le joueur, il recule
        { 
            _wall = true;

        }
        //if (other.gameObject.CompareTag("boundary"))
        //{
        //    transform.Rotate(this.transform.eulerAngles.x, this.transform.eulerAngles.y, this.transform.eulerAngles.z + 180);
        //}

    }

    void CalculateNewDirection()
    {
        if (Random.Range(0, 20) == 0)
        {
            _changingDirection = new Vector2(0, 0).normalized;
            _actualMovement = _changingDirection * _speed;
        }
        else
        { 
            _changingDirection = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f)).normalized;      // aléatoirise la direction dans laquelle l'ennemi va et transforme le vecteur en 1 unité
            _actualMovement = _changingDirection * _speed;                                                          // change le movement de l'ennemi
        }
    }
}
