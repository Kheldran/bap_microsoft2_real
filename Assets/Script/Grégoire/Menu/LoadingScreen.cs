﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingScreen : MonoBehaviour
{

    public GameObject loadingScreen;
    public GameObject loadingBar;
    public Text loadingPourcentageText;
    private bool isLoadingActive = false;
    AsyncOperation async = null;

    private RectTransform rectComponent;
    private float rotateSpeed = 200f;

    public void Loading(string name)
    {
        isLoadingActive = true;
        loadingScreen.SetActive(true);
        async = SceneManager.LoadSceneAsync(name);
        async.allowSceneActivation = false;
    }

    private void Start()
    {
        rectComponent = loadingBar.GetComponent<RectTransform>();
        loadingScreen.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (isLoadingActive)
        {
            rectComponent.Rotate(0f, 0f, rotateSpeed * Time.deltaTime);
            StartCoroutine(LoadingCoroutine());
        }
    }

    IEnumerator LoadingCoroutine()
    {
        while (!async.isDone)
        {
            if (async.progress < 0.9f)
            {
                //
            }
            else
            {
                async.allowSceneActivation = true;
            }

            yield return null;

        }


        yield return async;
    }
}
