﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

    public GameObject _playButton;
    public GameObject _quitButton;
    private GameObject _optionButton;
    private GameObject _optionButtonBack;
    public GameObject _optionMenu;
    public Slider _difficultySlider;
    Resolution[] resolutions;
    public Dropdown resolutionDropdown;

    public static bool _normalMode;
    public static bool _hardMode;


   // private ScrollRect resolutionDropdownList;


    //permet de lister les résolutions possibles et de set en premier la résolution native de l'écran de l'utilisateur
     void Start()
    {

        _normalMode = true;
        _optionButton = GameObject.FindGameObjectWithTag("optionButton");
        _optionButtonBack = GameObject.FindGameObjectWithTag("backButton");
       // resolutionDropdownList = resolutionDropdown.GetComponentInChildren<ScrollRect>();
        _optionMenu.SetActive(false);
        resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();

        int currentResolutionIndex = 0;
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);

            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }

        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
       // resolutionDropdownList.GetComponentInChildren<Scrollbar>().value = 0;
        resolutionDropdown.RefreshShownValue();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ToLvlOne();
        }
    }

    public void ToLvlOne()
    {
        SceneManager.LoadScene("Greg");
    }

    //permet de set la résolution d'écran sur le dropdown de l'UI
    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    //permet de lancer le jeu quan don press le bouton JOUER
    public void OnPlayPress()
    {
        SceneManager.LoadScene("Tuto");
    }

    //permet de quitter le jeu quand on clique sur le bouton QUIT
    public void OnButtonQuit()
    {
        Application.Quit();
    }

    //permet de rentrer dans le menu OPTION
    public void OptionButton()
    {
        _playButton.SetActive(false);
        
        _quitButton.SetActive(false);
        _optionMenu.SetActive(true);
        EventSystem.current.SetSelectedGameObject(_optionButtonBack);
        _optionButton.SetActive(false);
    }

    //permet de quitter le menu option
    public void OptionButtonQuit()
    {
        _playButton.SetActive(true);
        _optionButton.SetActive(true);
        _quitButton.SetActive(true);
        _optionMenu.SetActive(false);
        EventSystem.current.SetSelectedGameObject(_playButton);


    }

    //permet de changer la qualité du jeu dans les options
    public void QualityChange(int quality)
    {
        QualitySettings.SetQualityLevel(quality);
    }

    //permet de set le jeu en fullscreen ou non
    public void SetFullscreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }

    public void SetDifficulty()
    {
        if (_difficultySlider.value == 0)
        {
            _normalMode = true;
            _hardMode = false;
        }
        else if (_difficultySlider.value == 1)
        {
            _normalMode = false;
            _hardMode = true;
        }
        Debug.Log("hardMode : " + _hardMode + "normalMode : " + _normalMode);
    }
}
