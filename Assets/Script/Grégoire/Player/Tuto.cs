﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Tuto : MonoBehaviour {

    private Move _moveScript;

    public Image tutoText;

    public Sprite[] dialogSprite;

    public GameObject tuto2Trigger;
    public GameObject tuto3Trigger;
    public GameObject tuto4Trigger;
    public GameObject tuto5Trigger;
    public GameObject tuto6Trigger;

    private float timer;
    //private float timer2;

    public float TimerMax;

    private bool tuto1;
    private bool tuto2;
    private bool tuto3;
    private bool tuto4;
    private bool tuto5;
    private bool tuto6;




	// Use this for initialization
	void Start () {
        _moveScript = this.gameObject.GetComponent<Move>();
        timer = TimerMax;
        //timer2 = TimerMax;

        Debug.Log(SceneManager.GetActiveScene().name);
        if(SceneManager.GetActiveScene().name == "Greg")
        {
            tuto2Trigger = null;
            tuto3Trigger = null;
            tuto4Trigger = null;
            //tuto1 = false;
            //tuto2 = false;
            //tuto3 = false;
            //tuto4 = false;
            //tuto5 = false;
            //tuto6 = false;
        }
        else if (SceneManager.GetActiveScene().name == "Tuto")
        {
            tuto5Trigger = null;
            tuto6Trigger = null;
            tuto1 = true;

        }
	}
	
	// Update is called once per frame
	void Update () {

        if (_moveScript._lanternDuration <= _moveScript._lanternDurationMax * 0.25f && (!tuto1 && !tuto2 && !tuto3 && !tuto4 && !tuto5 && !tuto6))
        {
            tutoText.gameObject.SetActive(true);
            tutoText.sprite = dialogSprite[6];
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                timer = 0;
                tutoText.gameObject.SetActive(false);
            }
        }

        if (tuto1)
        {
            tutoText.gameObject.SetActive(true);
            timer -= Time.deltaTime;
            tutoText.sprite = dialogSprite[0];
            if (timer <= 0)
            {
                timer = 0;
                tutoText.gameObject.SetActive(false);
                tuto1 = false;
            }
        }
        if (tuto2)
        {
            tuto2Trigger.SetActive(false);
            tutoText.sprite = dialogSprite[1];
            timer -= Time.deltaTime;
            if(timer <= 0)
            {
                timer = 0;
                tutoText.gameObject.SetActive(false);
                tuto2 = false;

                //timer2 -= Time.deltaTime;
                //if (timer2 <= 0)
                //{
                //    tutoText.gameObject.SetActive(false);
                //}
            }
        }

        if (tuto3)
        {
            tuto3Trigger.SetActive(false);
            tutoText.sprite = dialogSprite[2];

            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                timer = 0;
                tutoText.gameObject.SetActive(false);
                tuto3 = false;

                //timer2 -= Time.deltaTime;
                //if(timer2 <=0)
                //{
                //    tutoText.gameObject.SetActive(false);
                //}
            }

        }

        if (tuto4)
        {
            tuto4Trigger.SetActive(false);
            tutoText.sprite = dialogSprite[3];
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                timer = 0;
                tutoText.gameObject.SetActive(false);
                tuto4 = false;

                //timer2 -= Time.deltaTime;
                //if (timer2 <= 0)
                //{
                //    tutoText.gameObject.SetActive(false);

                //}
            }
        }

        if (tuto5)
        {
            tuto5Trigger.SetActive(false);
            tutoText.sprite = dialogSprite[4];

            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                timer = 0;
                tutoText.gameObject.SetActive(false);
                tuto5 = false;

                //timer2 -= Time.deltaTime;
                //if(timer2 <=0)
                //{
                //    tutoText.gameObject.SetActive(false);
                //}
            }

        }

        if (tuto6)
        {
            tuto6Trigger.SetActive(false);
            tutoText.sprite = dialogSprite[5];

            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                timer = 0;
                tutoText.gameObject.SetActive(false);
                tuto6 = false;

                //timer2 -= Time.deltaTime;
                //if(timer2 <=0)
                //{
                //    tutoText.gameObject.SetActive(false);
                //}
            }

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("tuto2"))
        {
            tutoText.gameObject.SetActive(true);
            timer = TimerMax;
            //timer2 = TimerMax;
            tuto2 = true;

        }
        else if (collision.CompareTag("tuto3"))
        {
            tutoText.gameObject.SetActive(true);
            timer = TimerMax;
            //timer2 = TimerMax;
            tuto3 = true;
        }
        else if (collision.CompareTag("tuto4"))
        {
            tutoText.gameObject.SetActive(true);
            timer = TimerMax;
            //timer2 = TimerMax;
            tuto4 = true;
        }
        else if (collision.CompareTag("tuto5"))
        {
            tutoText.gameObject.SetActive(true);
            timer = TimerMax;
            //timer2 = TimerMax;
            tuto5 = true;
        }
        else if (collision.CompareTag("tuto6"))
        {
            tutoText.gameObject.SetActive(true);
            timer = TimerMax;
            //timer2 = TimerMax;
            tuto6 = true;
        }
    }
}
