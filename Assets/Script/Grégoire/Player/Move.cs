﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Move : MonoBehaviour {

    public float _moveSpeed = 15f; // Vitesse du joueur

    //public Rigidbody _rigidbody;

    public GameObject _weaponPosition;
    private GameObject _firearm;

    public GameObject _lantern;
    public bool _lanternOn { get; private set; }
    public float _lanternDuration { get; set; }
    public float _lanternDurationMax { get; private set; }
    public Slider _lanterSlider;
    public GameObject _lanternPosition1;
    public GameObject _lanternPosition2;
    public GameObject _lanternPosition3;
    public GameObject _lanternPosition4;
    public GameObject _lanternPosition5;
    public GameObject _lanternPosition6;
    public GameObject _lanternPosition7;
    public GameObject _lanternPosition8;
    private Quaternion _lanternStartRotation;
    private float _timerRotation;
    private bool _spriteChange;

    private Shoot _shootScript;
    private Weapons _weaponsScript;

    public bool _firstShoot = true;
    private bool _isFireShot;

    private _GameManager _gameManagerScript;
    public GameObject _gameManagerObject;

    public bool _canIStartFire;

    public Sprite _upLeft;
    public Sprite _upRight;
    public Sprite _downLeft;
    public Sprite _downRight;

    public GameObject _playerSkin;
    private Renderer _playerRender;

    private float invincibilityLength;
    private float invincibilityCounter;
    private float flashCounter = 0;
    private float flashLength = 0.1f;
    private bool invulnerability = false;
    private bool _isEnemyStoned;

    public bool _isPlayerInFire { get; private set; }

    public AkBank mainBank;
    private Day_Night dayNightScript;

    public GameObject endVideo;
    private bool isVideoSet;

    public GameObject canvas;

    private Animator _playerAnimator;
    private float lastXValue;
    private float lastYValue;
    private float _zeroTimer;

    // Use this for initialization
    void Start () {

		_gameManagerScript = _gameManagerObject.GetComponent<_GameManager> ();					// accède au script GameManager
		_playerRender = _playerSkin.GetComponent<Renderer> ();									// accède au renderer de l'object du joueur
		invincibilityLength = 2f;																// période d'invulnérabilité = 2 secondes
        _lanternOn = false;                                                                     //check si la lanterne est allumée
        _lantern.SetActive(false);
        _lanternDuration = 50f;                                                                // set la durée de la lanterne
        _lanternDurationMax = _lanternDuration;                                                 // set la durée maximum de la lanterne
        _lanternStartRotation = _lantern.transform.rotation;
        dayNightScript = _gameManagerObject.GetComponent<Day_Night>();
        dayNightScript._isDayOn = false;

        _playerAnimator = this.GetComponent<Animator>();

        //set la variable en null pdt le tuto
        if (SceneManager.GetActiveScene().name == "Tuto")
        {
            endVideo = null;
            canvas = null;
            this.GetComponent<RamasserObjetsStatues>().enabled = false;
            
        }
        else if (SceneManager.GetActiveScene().name == "Greg")
        {
            this.GetComponent<RamasserObjetsStatues>().enabled = true;
        }
    }

	// Update is called once per frame
	void Update () {
        //si le nom de la scène est différent de Tuto et qu'on a pas deja cherché la vidéo, trouve le gameObject vidéo et fait en sorte qu'on aille plus le rechercher
        if ((SceneManager.GetActiveScene().name == "Greg" || SceneManager.GetActiveScene().name == "Level1" )&& !isVideoSet)
        {
            endVideo = GameObject.FindWithTag("video");
            isVideoSet = true;
        }

        Vector3 _movement = Vector3.zero;
		_movement.x = Input.GetAxis ("JoystickHorizontal"); // prendre l'input de JoystickHorizontal pour les déplacement en x
		_movement.x = Input.GetAxis ("Horizontal"); // input clavier
		_movement.y = Input.GetAxis ("JoystickVertical");  // prendre l'input de JoystickVertical pour les déplacement en y
		_movement.y = Input.GetAxis ("Vertical");  //input clavier
		transform.position += _movement * Time.deltaTime * _moveSpeed; // bouger le joueur

		Rotating(Input.GetAxis ("JoystickHorizontal"),Input.GetAxis ("JoystickVertical")); //appelle la fontion Rotate()
        // Debug.Log("x : " + _movement.x + "y : " + _movement.y);

        // place la lanterne en fonciton de l'animation en cours

        if (_playerAnimator.GetBool("Down") == true)
        {
            _weaponPosition.transform.position = _lanternPosition7.transform.position;
        }
        if (_playerAnimator.GetBool("Right") == true)
        {
            _weaponPosition.transform.position = _lanternPosition5.transform.position;
        }
        if (_playerAnimator.GetBool("Left") == true)
        {
            _weaponPosition.transform.position = _lanternPosition6.transform.position;
        }
        if (_playerAnimator.GetBool("Up") == true)
        {
            _weaponPosition.transform.position = _lanternPosition8.transform.position;
        }


        if (_movement.y > 0.001f && _movement.x > 0.001f)
        {
            

            _playerAnimator.SetBool("Down", true);

            _playerAnimator.SetBool("Up", false);
            _playerAnimator.SetBool("Right", false);
            _playerAnimator.SetBool("Left", false);
            _playerAnimator.SetBool("IdleUpRight", false);
            _playerAnimator.SetBool("IdleUpLeft", false);
            _playerAnimator.SetBool("IdleDownRight", false);
            _playerAnimator.SetBool("IdleDownLeft", false);
            lastYValue = _movement.y;
            lastXValue = _movement.x;
            
        }
        else if (_movement.x >= 0.001f && _movement.y < -0.001f)
        {
            _playerAnimator.SetBool("Right", true);


            _playerAnimator.SetBool("Left", false);
            _playerAnimator.SetBool("Up", false);
            _playerAnimator.SetBool("Down", false);
            _playerAnimator.SetBool("IdleUpRight", false);
            _playerAnimator.SetBool("IdleUpLeft", false);
            _playerAnimator.SetBool("IdleDownRight", false);
            _playerAnimator.SetBool("IdleDownLeft", false);
            lastYValue = _movement.y;
            lastXValue = _movement.x;
            
        }
        else if (_movement.x <= -0.001f && _movement.y < -0.001f)
        {
            _playerAnimator.SetBool("Left", true);
            

            _playerAnimator.SetBool("Right", false);
            _playerAnimator.SetBool("Up", false);
            _playerAnimator.SetBool("Down", false);
            _playerAnimator.SetBool("IdleUpRight", false);
            _playerAnimator.SetBool("IdleUpLeft", false);
            _playerAnimator.SetBool("IdleDownRight", false);
            _playerAnimator.SetBool("IdleDownLeft", false);
            lastYValue = _movement.y;
            lastXValue = _movement.x;
            
        }
        else if (_movement.y > 0.001f && _movement.x <-0.001f)
        {
            
            _playerAnimator.SetBool("Up", true); // a mettre en true pour l'anim
            


            _playerAnimator.SetBool("Down", false);
            _playerAnimator.SetBool("Right", false);
            _playerAnimator.SetBool("Left", false);
            _playerAnimator.SetBool("IdleUpRight", false);
            _playerAnimator.SetBool("IdleUpLeft", false);
            _playerAnimator.SetBool("IdleDownRight", false);
            _playerAnimator.SetBool("IdleDownLeft", false); //a falser
            lastYValue = _movement.y;
            lastXValue = _movement.x;
            
        }
        else if (_movement.y > 0.001f && _movement.x == 0)
        {
            _playerAnimator.SetBool("Up", true); // a mettre en true pour l'anim
            

            _playerAnimator.SetBool("Down", false);
            _playerAnimator.SetBool("Right", false);
            _playerAnimator.SetBool("Left", false);
            _playerAnimator.SetBool("IdleUpRight", false);
            _playerAnimator.SetBool("IdleUpLeft", false);
            _playerAnimator.SetBool("IdleDownRight", false);
            _playerAnimator.SetBool("IdleDownLeft", false); //a falser
            lastYValue = _movement.y;
            lastXValue = _movement.x;

        }
        else if (_movement.y < -0.001f && _movement.x == 0)
        {
            _playerAnimator.SetBool("Up", false); // a mettre en true pour l'anim
            

            _playerAnimator.SetBool("Down", false);
            _playerAnimator.SetBool("Right", false);
            _playerAnimator.SetBool("Left", true);
          



            _playerAnimator.SetBool("IdleUpRight", false);
            _playerAnimator.SetBool("IdleUpLeft", false);
            _playerAnimator.SetBool("IdleDownRight", false);
            _playerAnimator.SetBool("IdleDownLeft", false); //a falser
            lastYValue = _movement.y;
            lastXValue = _movement.x;

        }
        else if (_movement.y == 0 && _movement.x > 0.001f)
        {
            _playerAnimator.SetBool("Up", false); 
            _playerAnimator.SetBool("Down", false);
            _playerAnimator.SetBool("Right", true);



            _playerAnimator.SetBool("Left", false);
            _playerAnimator.SetBool("IdleUpRight", false);
            _playerAnimator.SetBool("IdleUpLeft", false);
            _playerAnimator.SetBool("IdleDownRight", false);
            _playerAnimator.SetBool("IdleDownLeft", false); 
            lastYValue = _movement.y;
            lastXValue = _movement.x;

        }
        else if (_movement.y == 0 && _movement.x < -0.001f)
        {
            _playerAnimator.SetBool("Up", false); 
            _playerAnimator.SetBool("Down", false);
            _playerAnimator.SetBool("Right", false);
            _playerAnimator.SetBool("Left", true);


            _playerAnimator.SetBool("IdleUpRight", false);
            _playerAnimator.SetBool("IdleUpLeft", false);
            _playerAnimator.SetBool("IdleDownRight", false);
            _playerAnimator.SetBool("IdleDownLeft", false); 
            lastYValue = _movement.y;
            lastXValue = _movement.x;

        }



        if (_movement.x == 0 && _movement.y == 0)
        {
            _zeroTimer += Time.deltaTime;

            if (_playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("PlayerLeftAnim") && _zeroTimer > 0.08f)
            {
                Debug.Log(_zeroTimer);
                _playerAnimator.SetBool("Left", false);
                _playerAnimator.SetBool("IdleDownLeft", true);
                _weaponPosition.transform.position = _lanternPosition4.transform.position;
                _zeroTimer = 0;
            }
            else if (_playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("PlayerRightAnim") && _zeroTimer > 0.08f)
            {
                _playerAnimator.SetBool("Right", false);
                _playerAnimator.SetBool("IdleDownRight", true);
                _weaponPosition.transform.position = _lanternPosition3.transform.position;
                _zeroTimer = 0;
            }
            else if (_playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("upleftPlayer") && _zeroTimer > 0.08f)
            {
                _playerAnimator.SetBool("Up", false);
                _playerAnimator.SetBool("IdleUpLeft", true);
                _weaponPosition.transform.position = _lanternPosition1.transform.position;
                _zeroTimer = 0;
            }
            else if (_playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("uprightPlayer") && _zeroTimer > 0.08f)
            {
                _playerAnimator.SetBool("Down", false);
                _playerAnimator.SetBool("IdleUpRight", true);
                _weaponPosition.transform.position = _lanternPosition2.transform.position;
                _zeroTimer = 0;
            }

            /*_playerAnimator.SetBool("Right", false);
            _playerAnimator.SetBool("Left", false);
            _playerAnimator.SetBool("Up", false);
            _playerAnimator.SetBool("Down", false);
            if (lastYValue > 0.01f && lastXValue > 0.01f)
            {
                _playerAnimator.SetBool("IdleUpRight", true);
                _weaponPosition.transform.position = _lanternPosition2.transform.position;

            }
            else if (lastYValue < -0.01f && lastXValue > 0.01f && !_playerAnimator.GetBool("Left"))
            {
                _playerAnimator.SetBool("IdleDownRight", true);
                _weaponPosition.transform.position = _lanternPosition3.transform.position;

            }
            else if (lastYValue > 0.01f && lastXValue < 0.01f)
            {
                _playerAnimator.SetBool("IdleUpLeft", true);
                _weaponPosition.transform.position = _lanternPosition1.transform.position;      //change la position du gameobject portant la lanterne sur la position de la lanterne lorsque le sprite est en haut à gauche
            }
            else if (lastYValue < -0.01f && lastXValue < 0.01f && !_playerAnimator.GetBool("Right"))
            {
                _playerAnimator.SetBool("IdleDownLeft", true);
            }*/

        }
        else
        {
            _zeroTimer = 0;
        }

		//if (_isFireShot )
  //      {
		//	_shootScript._fireReload += Time.deltaTime;							// lance le temps de recharge de l'arme avant le prochain tir
		//	if (_shootScript._fireReload >= _shootScript._fireRate)
  //          {			                                                            // si le temps de recharge est supérieur a la vitesse de tir, permet de tirer a nouveau
		//	    _isFireShot = false;												// reset le tir.
		//    }
	 //   }
        print(Input.GetAxis("Light"));
        //en appuyant sur A, toggle la lanterne ON et OFF
        //if (Input.GetAxis("Light") == 1 && _lanternDuration >0) {     
        if (Input.GetKeyDown("joystick button 0") && _lanternDuration > 0)
        {
            _lanternOn = !_lanternOn;
            _lantern.SetActive(_lanternOn);
            dayNightScript._isDayOn = !dayNightScript._isDayOn;
        }
        // fais baisser la durée de la lanterne en fonction du temps
        if (_lanternOn) {
            _lanternDuration -= 1f * Time.deltaTime;
            _lanterSlider.value = _lanternDuration;


            // désactive la lanterne si plus de fuel 
            if (_lanternDuration <= 0)              
            {
                _lanternOn = false;
                _lantern.SetActive(false);
                _lanternDuration = 0;
                dayNightScript._isDayOn = false;
            }
        }

        if (_isPlayerInFire)                                //régénère la lanterne quand on est proche d'un feu
        {
            _lanternDuration += 3 * Time.deltaTime;
            _lanterSlider.value = _lanternDuration;
            if (_lanternDuration >= _lanternDurationMax)
            {
                _lanternDuration = _lanternDurationMax;
            }
        }

		if (invincibilityCounter > 0) {												

			invincibilityCounter -= Time.deltaTime;									//fais s'écouler la période d'invulnérabilité
			flashCounter -= Time.deltaTime;											// fais s'écouler la période d'activation/désactivation du renderer

			if (flashCounter <= 0) {												// si la période d'activation/désactivation du renderer <=0

				_playerRender.enabled = !_playerRender.enabled;						// désactive/active le renderer
				flashCounter = flashLength;											//reset la période d'activation/désactivation du renderer
			}

			if (invincibilityCounter <= 0) {										// si la durée d'invincibilité est écoulée

				_playerRender.enabled = true;										// active le renderer
				invulnerability=false;												// permet au joueur de se reprendre des dommages dans le trigger en dessous

			}	
		}
	}


	void OnTriggerEnter2D(Collider2D other) {

        if (other.CompareTag("enemy"))
        {
            _isEnemyStoned = other.GetComponent<EnemyMovement>()._amIStoned;        //check si le monstre est stun ou pas
        }

		if (invulnerability == false && !_isEnemyStoned) {																// vérifie si il y a invulnérabilité et si l'ennemi n'est pas stun
			if (other.gameObject.tag == "enemy" || other.gameObject.tag == "enemybullet" ) {			// si collision avec enemy ou les balles de enemy
			
				_gameManagerScript._playerLife -= 1;												//enlève 1 de vie au joueur
                BackgroundMusic.Instance.TakeAHit();                                                // lance le son du "aie" du player
				invincibilityCounter = invincibilityLength;											// démare le compteur d'invincibilité
				_playerRender.enabled = false;														// désactive le rendrer du joueur
				flashCounter = flashLength;															// active l'activation/désactivation du renderer
				invulnerability = true;																// fait en sorte que l'on ne se prène plus de dommages pdt l'invulnérabilité
			}
		}

        if (other.CompareTag("End"))
        {
            _gameManagerObject.GetComponent<LoadingScreen>().Loading("Greg");
            mainBank.UnloadBank(BackgroundMusic.Instance.gameObject);
        }

        if (other.CompareTag("Lvl1End"))
        {
            endVideo.GetComponent<UnityEngine.Video.VideoPlayer>().enabled = true;
            canvas.SetActive(false);
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("feu"))        // si le joueur rentre dans une zone feu de camp, les monstres ne le poursuive plus
        {
            _isPlayerInFire = true;

        }

        //if (other.CompareTag("AllumeFeu"))
        //{
        // _canIStartFire = true;          // donne au joueur la capacité d'allumer des feux après avoir ramasser l'allume feu dans le tuto
        // }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("feu"))    // si le joueur sort d'une zone feu de camp, les monstres dans la zone d'aggro le poursuive.
        {

            _isPlayerInFire = false;

        }
    }


    public void Rotating(float horizontalRotation, float verticalRotation)
	{

		/*
		 
		 if (horizontalRotation != 0 || verticalRotation != 0) {

		// Create a new vector of the horizontal and vertical inputs.
		Vector3 targetDirection = new Vector3(horizontalRotation, 0f, verticalRotation);

		// Create a rotation based on this new vector assuming that up is the global y axis.
		Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);
		// Create a rotation that is an increment closer to the target rotation from the player's rotation.
			Quaternion newRotation = Quaternion.Lerp(_rigidbody.rotation, targetRotation, 0.5f * Time.deltaTime);

		// Change the players rotation to this new rotation.
		_rigidbody.MoveRotation(newRotation);

		*/

			
		if (horizontalRotation != 0.0 || verticalRotation != 0.0) {                             // Ne relance pas la fonction a chaque update et ne reset donc pas la rotation du joueur.
      
            var angle = Mathf.Atan2(verticalRotation, horizontalRotation) * Mathf.Rad2Deg;      // calcule la tan de l'input du joystick et la convertit en degré 

            if (angle < 180 && angle >= 90)                                                     //si on pointe le joystick en haut à gauche, change le sprite du perso pour le sprite en haut à gauche 
            {
                this.GetComponent<SpriteRenderer>().sprite = _upLeft;
                
            }
            if (angle < 90 && angle >= 0)                                                       //pareil qu'au dessus pour le sprite en haut à droite
            {
                this.GetComponent<SpriteRenderer>().sprite = _upRight;
                
            }
            if (angle >= -90 && angle < 0)                                                       //pareil qu'au dessus pour le sprite en bas à droite            
            {
                this.GetComponent<SpriteRenderer>().sprite = _downRight;

            }
            if (angle > -180 && angle < -90)                                                       //pareil qu'au dessus pour le sprite en bas à gauche
            {
                this.GetComponent<SpriteRenderer>().sprite = _downLeft;
                _weaponPosition.transform.position = _lanternPosition4.transform.position;
            }

            _weaponPosition.transform.rotation = Quaternion.AngleAxis(90f - angle, Vector3.back);               // change la rotation de l'objet qui tient selon l'angle calculé au dessus et dans la direction z.

            /* _weaponsScript = this.GetComponent<Weapons> ();							//prends le script Weapons
             if (_weaponsScript._isWeaponEquiped) {									//si l'on a une arme, la fonction continue
                 _firearm = GameObject.FindGameObjectWithTag ("equipedWeapon");		//trouve l'arme que l'on a équipée
                 _shootScript = _firearm.GetComponent<Shoot> ();						// trouve son script


                 //script pour tirer lorsque l'on maintien le joystick de rotation
                 if (_shootScript._fireReload >= _shootScript._fireRate || _firstShoot){ 		//si le rechargement du tir est égale a la vitesse du tir, applique le if ou si c'est le premier tir de l'arme.

                     // clone le gameObject de la balle de l'arme
                     GameObject _shotClone = Instantiate (_shootScript._shoot, _weaponPosition.transform.position, _weaponPosition.transform.rotation) as GameObject;
                     _shootScript._fireReload = 0;		//reset le timer du rechargement du tir de l'arme.
                     _isFireShot = true;					//permet d'enclencher le timer du rechargmeent du tir de l'arme
                     _firstShoot = false;				//enleve la condition du premier shoot.
                 }
        }*/
     }
	}

}
